package com.spring.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpingbootDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpingbootDockerApplication.class, args);
	}

}
